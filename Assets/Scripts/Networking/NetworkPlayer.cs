using System.Collections;
using System.Collections.Generic;
using Character;
using Interactions;
using Mirror;
using UnityEngine;

public class NetworkPlayer : NetworkBehaviour
{
    public override void OnStartAuthority()
    {
        if (isLocalPlayer)
        {
            CharacterManager.Instance.RequestSpawnCharacter();
            InteractionManager.Instance.RequestInteractionController();
        }
    }
}