using Mirror;

namespace Networking
{
    public static class ClientOwnershipUtility
    {
        public static Type GetOwnershipType(NetworkBehaviour networkBehaviour)
        {
            if (networkBehaviour.isOwned) return Type.Owner;
            if (networkBehaviour.isServer) return Type.Server;
            return Type.Observer;
        }
        
        public enum Type
        {
            Owner,
            Server,
            Observer
        }
    }
}