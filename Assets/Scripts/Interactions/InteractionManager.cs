using System;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

namespace Interactions
{
    public class InteractionManager : NetworkBehaviour
    {
        #region Singleton

        public static InteractionManager Instance
        {
            get
            {
                if (_instance != null) { return _instance; }

                var newInteractionManager = new GameObject("InteractionManager");
                newInteractionManager.AddComponent<InteractionManager>();
                _instance = newInteractionManager.GetComponent<InteractionManager>();
                return _instance;
            }
        }
        private static InteractionManager _instance;

        private void Awake()
        {
            if (_instance != null & _instance != this)
            {
                Destroy(this);
                return;
            }

            _instance = this;
        }

        #endregion

        [SerializeField] private GameObject _interactionControllerPrefab;
        private Dictionary<int, GameObject> _spawnedInteractionControllers = new Dictionary<int, GameObject>();

        [Command(requiresAuthority = false)]
        public void RequestInteractionController(NetworkConnectionToClient sender = null)
        {
            if (sender == null)
            {
                Debug.LogError("Sender was null!");
                return;
            }
            
            CreateInteractionController(sender);
        }

        [Server]
        private void CreateInteractionController(NetworkConnectionToClient networkConnection)
        {
            var newInteractionController = Instantiate(_interactionControllerPrefab);
            newInteractionController.name = $"InteractionController{networkConnection.connectionId}";
            NetworkServer.Spawn(newInteractionController, networkConnection);
            
            _spawnedInteractionControllers.Add(networkConnection.connectionId, newInteractionController);
            Debug.Log($"Spawned new InteractionController for {networkConnection.connectionId}");
        }
    }
}