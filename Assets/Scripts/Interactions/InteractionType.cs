namespace Interactions
{
    public enum InteractionType
    {
        PrimaryKey,
        SecondaryKey,
        PrimaryKeyHold,
        SecondaryKeyHold
    }
}