namespace Interactions.Interactable
{
    public interface IInteractable
    {
        public InteractionType Bind { get; }
        public void Interaction();
    }
}