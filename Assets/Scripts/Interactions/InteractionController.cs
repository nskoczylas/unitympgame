using System;
using Interactions.Interactable;
using Mirror;
using UnityEngine;

namespace Interactions
{
    public class InteractionController : NetworkBehaviour
    {
        [SerializeField] private Transform _raycastOrigin;
        [SerializeField] private float _raycastMaxDistance;
        [SerializeField] private LayerMask _raycastLayerMask;

        private bool _isActive = false;
        private Controls _controls;

        private RaycastHit _raycastHit;
        private GameObject _previousInteractable = null;
        private GameObject _currentInteractable = null;

        
        public override void OnStartAuthority()
        {
            if (!isOwned)
            {
                _isActive = false;
                this.enabled = false;
                gameObject.SetActive(false);
                return;
            }
            AttachToActiveCamera();
        }

        private void Update()
        {
            if (_isActive)
            {
                CheckForInteractables();
            }
        }

        public static void Create()
        {
            var controller = new GameObject();
            controller.AddComponent<InteractionController>();
        }

        private void AttachToActiveCamera()
        {
            var cameraTransform = Camera.main.gameObject.transform;

            if (cameraTransform == null)
            {
                Debug.LogError("Unable to get active camera! Turning off...");
                gameObject.SetActive(false);
                return;
            }

            _raycastOrigin = cameraTransform;
            _isActive = true;
        }

        private void CheckForInteractables()
        {
            var ray = new Ray(_raycastOrigin.position, _raycastOrigin.forward);

            if (Physics.Raycast(ray, out _raycastHit, _raycastMaxDistance, _raycastLayerMask))
            {
                var hitObject = _raycastHit.transform.gameObject;
                if (hitObject.GetComponent<InteractableController>())
                {
                    ProcessHitInfo(hitObject);
                    return;
                }
            }
            
            ProcessHitInfo(null);
        }

        private void ProcessHitInfo(GameObject hitGameObject)
        {
            _currentInteractable = hitGameObject;
            if (_currentInteractable != _previousInteractable) ChangePreviousInteractable();
        }

        private void ChangePreviousInteractable()
        {
            if (IsPreviousInteractableEqualCurrent()) { return; }
            
            if (_previousInteractable != null) OnLastHit(_previousInteractable);
            _previousInteractable = _currentInteractable;
            
            if (_currentInteractable != null) OnFirstHit(_currentInteractable); 
        }

        private bool IsPreviousInteractableEqualCurrent()
        {
            return _previousInteractable == _currentInteractable;
        }

        private void OnFirstHit(GameObject hitGameObject)
        {
            // Enable interaction with interactable object.
            Debug.Log($"Can now interact with {hitGameObject}");
        }

        private void OnLastHit(GameObject hitGameObject)
        {
            // Disable interaction with interactable object.
            Debug.Log($"Can no longer interact with {hitGameObject}");
        }
    }
}