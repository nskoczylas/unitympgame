using System;
using UnityEngine;
using System.Collections.Generic;
using Character.Components;
using Character.Components.ActivationStrategy;
using Mirror;
using Networking;

namespace Character
{
    public class CharacterComposer : NetworkBehaviour
    {
        private List<ICharacterComponent> _characterComponents = new List<ICharacterComponent>();
        private List<IComponentActivationStrategy> _distributionStrategies = new List<IComponentActivationStrategy>();

        #region Initialization

        private void Awake()
        {
            InitializeComponentList();
            InitializeStrategyList();
        }

        private void InitializeComponentList()
        {
            var characterComponents = GetComponents<ICharacterComponent>();
            if (characterComponents == null)
            {
                Debug.LogError($"{gameObject.name} character has been created without any components!");
                return;
            }

            foreach (var component in characterComponents)
            {
                _characterComponents.Add(component);
            }
        }

        private void InitializeStrategyList()
        {
            _distributionStrategies.Add(new OwnerActivationStrategy());
            _distributionStrategies.Add(new SharedActivationStrategy());
            _distributionStrategies.Add(new ServerActivationStrategy());
        }

        #endregion

        [ClientRpc]
        public void RpcEnableCharacter()
        {
            var ownership = ClientOwnershipUtility.GetOwnershipType(this);
            ActivateComponents(CharacterComponentType.ServerOnly, ownership);
            ActivateComponents(CharacterComponentType.Shared, ownership);
            ActivateComponents(CharacterComponentType.OwnerOnly, ownership);
        }

        [ClientRpc]
        public void RpcDisableCharacter()
        {
            var ownership = ClientOwnershipUtility.GetOwnershipType(this);
            DeactivateComponents(CharacterComponentType.ServerOnly, ownership);
            DeactivateComponents(CharacterComponentType.Shared, ownership);
            DeactivateComponents(CharacterComponentType.OwnerOnly, ownership);
        }

        private void ActivateComponents(CharacterComponentType wantedType, ClientOwnershipUtility.Type ownershipType)
        {
            foreach (var component in _characterComponents)
            { 
                ExecuteValidActivationStrategy(component, wantedType, ownershipType);
            }
        }
        
        private void DeactivateComponents(CharacterComponentType wantedType, ClientOwnershipUtility.Type ownershipType)
        {
            foreach (var component in _characterComponents)
            {
                ExecuteValidDeactivationStrategy(component, wantedType, ownershipType);
            }
        }

        private void ExecuteValidActivationStrategy(ICharacterComponent component, CharacterComponentType wantedType, ClientOwnershipUtility.Type ownershipType)
        {
            foreach (var strategy in _distributionStrategies)
            {
                if (strategy.Accept(component.ComponentType, wantedType))
                {
                    strategy.Activate(component, ownershipType);
                }
            }
        }
        
        private void ExecuteValidDeactivationStrategy(ICharacterComponent component, CharacterComponentType wantedType, ClientOwnershipUtility.Type ownershipType)
        {
            foreach (var strategy in _distributionStrategies)
            {
                if (strategy.Accept(component.ComponentType, wantedType))
                {
                    strategy.Deactivate(component, ownershipType);
                }
            }
        }
    }
}