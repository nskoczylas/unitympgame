using Networking;

namespace Character.Components.ActivationStrategy
{
    public class OwnerActivationStrategy : IComponentActivationStrategy
    {
        public bool Accept(CharacterComponentType componentType, CharacterComponentType wantedType)
        {
            return componentType == CharacterComponentType.OwnerOnly && wantedType == CharacterComponentType.OwnerOnly;
        }
        
        public void Activate(ICharacterComponent characterComponent, ClientOwnershipUtility.Type ownershipType)
        {
            if (ownershipType == ClientOwnershipUtility.Type.Owner)
            {
                characterComponent.EnableComponent();
            }
            else
            {
                characterComponent.DisableComponent();
            }
        }

        public void Deactivate(ICharacterComponent characterComponent, ClientOwnershipUtility.Type ownershipType)
        {
            characterComponent.DisableComponent();
        }
    }
}
