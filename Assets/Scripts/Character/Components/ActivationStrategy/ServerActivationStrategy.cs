using Networking;

namespace Character.Components.ActivationStrategy
{
    public class ServerActivationStrategy : IComponentActivationStrategy
    {
        public bool Accept(CharacterComponentType componentType, CharacterComponentType wantedType)
        {
            return componentType == CharacterComponentType.ServerOnly && wantedType == CharacterComponentType.ServerOnly;
        }
        
        public void Activate(ICharacterComponent characterComponent, ClientOwnershipUtility.Type ownershipType)
        {
            if (ownershipType == ClientOwnershipUtility.Type.Server)
            {
                characterComponent.EnableComponent();
            }
            else
            {
                characterComponent.DisableComponent();
            }
        }

        public void Deactivate(ICharacterComponent characterComponent, ClientOwnershipUtility.Type ownershipType)
        {
            characterComponent.DisableComponent();
        }
    }
}