using Networking;

namespace Character.Components.ActivationStrategy
{
    public class SharedActivationStrategy : IComponentActivationStrategy
    {
        public bool Accept(CharacterComponentType componentType, CharacterComponentType wantedType)
        {
            return componentType == CharacterComponentType.Shared && wantedType == CharacterComponentType.Shared;
        }

        public void Activate(ICharacterComponent characterComponent, ClientOwnershipUtility.Type ownershipType)
        {
            characterComponent.EnableComponent();
        }

        public void Deactivate(ICharacterComponent characterComponent, ClientOwnershipUtility.Type ownershipType)
        {
            characterComponent.DisableComponent();
        }
    }
}
