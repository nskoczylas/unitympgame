using Networking;

namespace Character.Components.ActivationStrategy
{
    public interface IComponentActivationStrategy
    {
        public bool Accept(CharacterComponentType componentType, CharacterComponentType wantedType);

        public void Activate(ICharacterComponent characterComponent, ClientOwnershipUtility.Type ownershipType);
        public void Deactivate(ICharacterComponent characterComponent, ClientOwnershipUtility.Type ownershipType);
    }
}