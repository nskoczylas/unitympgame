using UnityEngine;

namespace Character.Components
{
    public class CharacterAnimationController : MonoBehaviour, ICharacterComponent
    {
        #region ICharacterComponent
        
        public CharacterComponentType ComponentType { get { return CharacterComponentType.OwnerOnly; } }

        public void EnableComponent()
        {
            this.enabled = true;
        }

        public void DisableComponent()
        {
            this.enabled = false;
        }

        public void RemoveComponent()
        {
            Destroy(this);
        }

        #endregion
    
        [SerializeField] private Animator _animator;
        [SerializeField] private CharacterMovementController _characterMovementController;

        private void Update()
        {
            _animator.SetFloat("VelocityX", _characterMovementController.MoveVelocity.x);
            _animator.SetFloat("VelocityZ", _characterMovementController.MoveVelocity.y);
            _animator.SetFloat("Velocity", _characterMovementController.MoveVelocity.magnitude);
        }
    }
}
