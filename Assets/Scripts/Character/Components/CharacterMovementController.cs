using UnityEngine;

namespace Character.Components
{
    public class CharacterMovementController : MonoBehaviour, ICharacterComponent
    {
        #region ICharacterComponent
        
        public CharacterComponentType ComponentType { get { return CharacterComponentType.OwnerOnly; } }

        public void EnableComponent()
        {
            this.enabled = true;
        }

        public void DisableComponent()
        {
            this.enabled = false;
        }

        public void RemoveComponent()
        {
            Destroy(this);
        }

        #endregion
    
        public Vector2 MoveVelocity { get { return _moveVelocity; } }
        private Vector2 _moveVelocity;

        public bool IsSliding { get { return _isSliding; } }
    
        private Vector2 _moveInput;

        [Header("Settings:")]
        [SerializeField] private float _moveSpeed;
        [SerializeField] private float _moveSmoothTime;
        [SerializeField] private float _characterGravity;
        [SerializeField] private float _maxDistanceToAdjustToSlope;

        [Header("References:")]
        [SerializeField] private CharacterController _controller;
        [SerializeField] private CharacterGroundProbe _groundProbe;

        private Vector2 _moveInputVector;
        private Vector2 _moveInputVectorVelocity;
        private Vector2 _currentMoveInputVector;

        private float _moveGravity;
        private Vector3 _moveDirection;

        private bool _isSliding;
        
        public void SetMoveInput(Vector2 moveInput)
        {
            _moveInput = moveInput;
        }

        public void SetMoveSpeed(float moveSpeed)
        {
            _moveSpeed = moveSpeed;
        }

        private void Update()
        {
            Move();
        }

        private void Move()
        {
            _moveInputVector = _moveInput * _moveSpeed;
            _currentMoveInputVector = Vector2.SmoothDamp(_currentMoveInputVector, _moveInputVector, ref _moveInputVectorVelocity, _moveSmoothTime);

            _moveDirection = transform.forward * _currentMoveInputVector.y + transform.right * _currentMoveInputVector.x;

            _moveVelocity.x = _currentMoveInputVector.x;
            _moveVelocity.y = _currentMoveInputVector.y;
        
        
            ProcessGrounding();
            _moveDirection.y += _moveGravity;
            ApplySliding();
        
            _controller.Move(_moveDirection * Time.deltaTime);
        }

        private void ProcessGrounding()
        {
            if (_groundProbe.IsGrounded)
            {
                ApplyGroundedLogic();
            }
            else
            {
               ApplyNotGroundedLogic();
            }
        }

        private void ApplyGroundedLogic()
        {
            if (_groundProbe.GroundAngleRay < _controller.slopeLimit)
            {
                _isSliding = false;
                ApplyGroundSlopeCorrections();
            }
            else
            {
                _isSliding = true;
            }
        }

        private void ApplyGroundSlopeCorrections()
        {
            if (_groundProbe.GroundAngleRay == 0)
            {
                _moveGravity = -0.5f;
            }
            else
            {
                if (_groundProbe.GroundDistance < _maxDistanceToAdjustToSlope) AdjustMoveVectorToSlope();
                _moveGravity = _moveDirection.y - 0.1f;
            }
        }

        private void ApplyNotGroundedLogic()
        {
            _isSliding = false;
            _moveGravity -= _characterGravity * Time.deltaTime;
        }

        private void ApplySliding()
        {
            if (_isSliding)
            {
                _moveDirection = GetSlideDirection();
                _moveGravity -= _characterGravity * Time.deltaTime;
                _moveDirection.y += _moveGravity;
            }
        }

        private Vector3 GetSlideDirection()
        {
            return Vector3.ProjectOnPlane(new Vector3(0, _moveGravity, 0), _groundProbe.GroundCheckSphereHitNormal);
        }

        private void AdjustMoveVectorToSlope()
        {
            var adjustedMoveDirection = _groundProbe.GroundRotationSphere * _moveDirection;
            if (adjustedMoveDirection.y < 0)
            {
                _moveDirection = adjustedMoveDirection;
            }
        }
    }
}