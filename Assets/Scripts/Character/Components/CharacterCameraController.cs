using Cinemachine;
using UnityEngine;

namespace Character.Components
{
    public class CharacterCameraController : MonoBehaviour, ICharacterComponent
    {
        #region ICharacterComponent
        
        public CharacterComponentType ComponentType { get { return CharacterComponentType.OwnerOnly; } }

        public void EnableComponent()
        {
            _cameraRig.gameObject.SetActive(true);
            this.enabled = true;
        }

        public void DisableComponent()
        {
            _cameraRig.gameObject.SetActive(false);
            this.enabled = false;
        }

        public void RemoveComponent()
        {
            _cameraRig.gameObject.SetActive(false);
            Destroy(this);
        }

        #endregion


        #region Properties
    
        public float HeadRotation { get { return _headRotation; } }
        public float NeckRotation { get { return _neckRotation; } }
        public float ChestRotation { get { return _chestRotation; } }

        private float _headRotation;
        private float _neckRotation;
        private float _chestRotation;
    
        #endregion

        [Header("References / System")]
        [SerializeField] private Transform _cameraRig;
        [SerializeField] private Transform _cameraArm;
        [SerializeField] private Transform _cameraSocket;
        [SerializeField] private CinemachineVirtualCamera _virtualCamera;
    
        [Header("References / Visualisation")]
        [SerializeField] private Transform _headBone;
        [SerializeField] private Transform _neckBone;
        [SerializeField] private Transform _upperChestBone;

        [Header("Settings")]
        [SerializeField] private Vector2 _rotationXClamp;
        [SerializeField] private Vector2 _chestRotationXThreshold;

        private Vector2 _lookInput;

        private float _rotationX;
        private float _chestRotationX;

        private void Awake()
        {
            _rotationX = _headBone.localRotation.x;
            _chestRotationX = _upperChestBone.localRotation.x;
        }

        private void Update()
        {
            Look();
        }

        private void LateUpdate()
        {
            LookVisuals();
        }

        private void Look()
        {
            _rotationX -= _lookInput.y;
            _rotationX = Mathf.Clamp(_rotationX, _rotationXClamp.x, _rotationXClamp.y);

            if (_rotationX < _chestRotationXThreshold.x)
            {
                _headRotation = _chestRotationXThreshold.x / 2;
                _neckRotation = _chestRotationXThreshold.x / 2;
                _chestRotation = _rotationX - _chestRotationXThreshold.x;

                _cameraSocket.localRotation = Quaternion.Euler(_headRotation, 0, 0);
                _cameraArm.localRotation = Quaternion.Euler(_neckRotation, 0, 0);
                _cameraRig.localRotation = Quaternion.Euler(_chestRotation, 0, 0);
            }

            if (_rotationX > _chestRotationXThreshold.x && _rotationX < _chestRotationXThreshold.y)
            {
                _headRotation = _rotationX / 2;
                _neckRotation = _rotationX / 2;
                _chestRotation = 0;
            
                _cameraSocket.localRotation = Quaternion.Euler(_headRotation, 0, 0);
                _cameraArm.localRotation = Quaternion.Euler(_neckRotation, 0, 0);
                _cameraRig.localRotation = Quaternion.Euler(_chestRotation, 0, 0);
            }

            if (_rotationX > _chestRotationXThreshold.y)
            {
                _headRotation = _chestRotationXThreshold.y / 2;
                _neckRotation = _chestRotationXThreshold.y  / 2;
                _chestRotation = _rotationX - _chestRotationXThreshold.y;
            
                _cameraSocket.localRotation = Quaternion.Euler(_headRotation, 0, 0);
                _cameraArm.localRotation = Quaternion.Euler(_neckRotation, 0, 0);
                _cameraRig.localRotation = Quaternion.Euler(_chestRotation, 0, 0);
            }

            transform.rotation *= Quaternion.Euler(0, _lookInput.x, 0);
        }

        private void LookVisuals()
        {
            _headBone.localRotation = Quaternion.Euler(_headRotation, 0, 0);
            _neckBone.localRotation = Quaternion.Euler(_neckRotation, 0, 0);
            _upperChestBone.localRotation = Quaternion.Euler(_chestRotation, 0, 0);
        }

        public void SetLookInput(Vector2 lookInput)
        {
            _lookInput = lookInput;
        }
    }
}