namespace Character.Components
{
    public interface ICharacterComponent
    {
        public CharacterComponentType ComponentType { get; }
        public void EnableComponent();
        public void DisableComponent();
        public void RemoveComponent();
    }

    public enum CharacterComponentType
    {
        OwnerOnly,
        Shared,
        ServerOnly
    }
}