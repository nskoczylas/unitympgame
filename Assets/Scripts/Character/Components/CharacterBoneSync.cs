using Mirror;
using UnityEngine;

namespace Character.Components
{
    public class CharacterBoneSync : NetworkBehaviour, ICharacterComponent
    {
        #region ICharacterComponent
        
        public CharacterComponentType ComponentType { get { return CharacterComponentType.Shared; } }

        public void EnableComponent()
        {
            this.enabled = true;
        }

        public void DisableComponent()
        {
            this.enabled = false;
        }

        public void RemoveComponent()
        {
            Destroy(this);
        }

        #endregion
    
        [SerializeField] private CharacterCameraController _characterCameraController;
        [SerializeField] private Transform _headBone;
        [SerializeField] private Transform _neckBone;
        [SerializeField] private Transform _upperChestBone;
    
        private Vector3 _currentBoneRotations = Vector3.zero;
        private Vector3 _lastBoneRotations = Vector3.zero;

        [SyncVar]
        public Vector3 BoneRotations = Vector3.zero;

        private void Update()
        {
            if (!isOwned) { return; }
        
            GetBoneRotations();
            ProcessBoneRotations();
        }

        private void LateUpdate()
        {
            if (isOwned) { return; }
            ApplyBoneRotations();
        }

        private void GetBoneRotations()
        {
            _currentBoneRotations.x = _characterCameraController.HeadRotation;
            _currentBoneRotations.y = _characterCameraController.NeckRotation;
            _currentBoneRotations.z = _characterCameraController.ChestRotation;
        }

        private void ProcessBoneRotations()
        {
            if (_currentBoneRotations != _lastBoneRotations) { SetBoneRotations(_currentBoneRotations); }
            _lastBoneRotations = _currentBoneRotations;
        }

        private void ApplyBoneRotations()
        {
            _headBone.localRotation = Quaternion.Euler(BoneRotations.x, 0, 0);
            _neckBone.localRotation = Quaternion.Euler(BoneRotations.y, 0, 0);
            _upperChestBone.localRotation = Quaternion.Euler(BoneRotations.z, 0, 0);
        }
    
        private void SetBoneRotations(Vector3 boneRotations)
        {
            BoneRotations = boneRotations;
        }
    }
}
