using UnityEngine;

namespace Character.Components
{
    public class CharacterGroundProbe : MonoBehaviour, ICharacterComponent
    {
        #region ICharacterComponent

        public CharacterComponentType ComponentType { get { return CharacterComponentType.Shared; } }

        public void EnableComponent()
        {
            this.enabled = true;
        }

        public void DisableComponent()
        {
            this.enabled = false;
        }

        public void RemoveComponent()
        {
            Destroy(this);
        }

        #endregion
        
        [Header("GROUND DETECTION / SHARED")]
        public Vector3 GroundCheckOriginOffset = Vector3.zero;
        public LayerMask GroundCheckLayerMask;

        [Header("GROUND DETECTION / SPHERE")] 
        public float GroundCheckSphereRadius;
        public float GroundCheckSphereMaxDistance;
        public float GroundAngleCheckSphereMaxDistance;

        [Header("GROUND DETECTION / RAY")] 
        public float GroundCheckRayMaxDistance;
    
        #region Properties
    
        // IsGrounded:
        public bool IsGrounded { get { return _isGrounded; } }
        private bool _isGrounded;
    
        // Ground distance:
        public float GroundDistance { get { return _groundDistance; } }
        private float _groundDistance;
    
        // Ground rotation:
        public Quaternion GroundRotationSphere { get { return _groundRotationSphere; } }
        private Quaternion _groundRotationSphere;
    
        public Quaternion GroundRotationRay { get { return _groundRotationRay; } }
        private Quaternion _groundRotationRay;
    
        // Ground angle:
        public float GroundAngleSphere { get { return _groundAngleSphere; } }
        private float _groundAngleSphere;
    
        public float GroundAngleRay { get { return _groundAngleRay; } }
        private float _groundAngleRay;
    
        // HIT Normal:
        public Vector3 GroundCheckSphereHitNormal { get { return _groundCheckSphereHitNormal; } }
        private Vector3 _groundCheckSphereHitNormal;
    
        public Vector3 GroundCheckRayHitNormal { get { return _groundCheckRayHitNormal; } }
        private Vector3 _groundCheckRayHitNormal;
    
        #endregion

        // Ray:
        private Ray _groundCheckRay;
    
        // Raycast hits:
        private RaycastHit _groundCheckSphereHit;
        private RaycastHit _groundCheckRayHit;

        private void Update()
        {
            UpdateRay();
            GroundCheckSphere();
            GroundCheckRay();
        }

        private void UpdateRay()
        {
            _groundCheckRay = new Ray(transform.position + GroundCheckOriginOffset, Vector3.down);
        }

        private void GroundCheckSphere()
        {
            if (Physics.SphereCast(_groundCheckRay, GroundCheckSphereRadius, out _groundCheckSphereHit, GroundCheckSphereMaxDistance, GroundCheckLayerMask))
            {
                _isGrounded = true;
                _groundRotationSphere = Quaternion.FromToRotation(Vector3.up, _groundCheckSphereHit.normal);
                _groundAngleSphere = Vector3.Angle(_groundCheckSphereHit.normal, Vector3.up);
                _groundCheckSphereHitNormal = _groundCheckSphereHit.normal;
            }
            else
            {
                _isGrounded = false;
            
                if (Physics.SphereCast(_groundCheckRay, GroundCheckSphereRadius, out _groundCheckSphereHit, GroundAngleCheckSphereMaxDistance, GroundCheckLayerMask))
                {
                    _groundRotationSphere = Quaternion.FromToRotation(Vector3.up, _groundCheckSphereHit.normal);
                    _groundAngleSphere = Vector3.Angle(_groundCheckSphereHit.normal, Vector3.up);
                    _groundCheckSphereHitNormal = _groundCheckSphereHit.normal;
                }
                else
                {
                    _groundRotationSphere = Quaternion.identity;
                    _groundAngleSphere = 0f;
                }
            }
        }
    
        private void GroundCheckRay()
        {
            if (Physics.Raycast(_groundCheckRay, out _groundCheckRayHit, GroundCheckRayMaxDistance, GroundCheckLayerMask))
            {
                _groundRotationRay = Quaternion.FromToRotation(Vector3.up, _groundCheckRayHit.normal);
                _groundAngleRay = Vector3.Angle(_groundCheckRayHit.normal, Vector3.up);
                _groundDistance = Vector3.Distance(transform.position + GroundCheckOriginOffset, _groundCheckRayHit.point);
                _groundCheckRayHitNormal = _groundCheckRayHit.point;
            }
            else
            {
                _groundRotationRay = Quaternion.identity;
                _groundAngleRay = 0f;
            }
        }

        #region Gizmos

        [Header("GROUND DETECTION / GIZMOS")]
        [SerializeField] private bool _drawRayOrigin;
        [SerializeField] private float _rayOriginRadius;
        [SerializeField] private Color _rayOriginColor;
        [SerializeField] private bool _drawGroundCheckSphere;
        [SerializeField] private bool _drawGroundCheckRay;

        private void OnDrawGizmos()
        {
            if (_drawRayOrigin) DrawRayOrigin();
            if (_drawGroundCheckSphere) DrawGroundCheckSphere();
            if (_drawGroundCheckRay) DrawGroundCheckRay();
        }

        private void DrawRayOrigin()
        {
            Gizmos.color = _rayOriginColor;
            Gizmos.DrawWireSphere(transform.position + GroundCheckOriginOffset, _rayOriginRadius);
        }

        private void DrawGroundCheckSphere()
        {
            Gizmos.color = _isGrounded ? Color.green : Color.red;

            Vector3 wireSphereCenter = transform.position + GroundCheckOriginOffset;
            wireSphereCenter.y -= GroundCheckSphereMaxDistance;
            Gizmos.DrawWireSphere(wireSphereCenter, GroundCheckSphereRadius);
        }

        private void DrawGroundCheckRay()
        {
            Gizmos.color = _isGrounded ? Color.green : Color.red;
            Gizmos.DrawRay(_groundCheckRay);
        }

        #endregion
    }
}