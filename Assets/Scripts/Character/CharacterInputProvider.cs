using Character.Components;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace Character
{
    public class CharacterInputProvider : MonoBehaviour, ICharacterComponent
    {
        #region ICharacterComponent
        
        public CharacterComponentType ComponentType { get { return CharacterComponentType.OwnerOnly; } }

        public void EnableComponent()
        {
            this.enabled = true;
        }

        public void DisableComponent()
        {
            this.enabled = false;
        }

        public void RemoveComponent()
        {
            Destroy(this);
        }

        #endregion
    
        public UnityEvent<Vector2> OnMoveInput;
        public UnityEvent<Vector2> OnLookInput;

        private Controls _controls;
        private Controls.CharacterActions _actions;

        private bool _hasBeenInitialized = false;
        private bool _hasBeenEnabled = false;

        public void InitializeControls()
        {
            _controls = new Controls();
            _actions = _controls.Character;
            _hasBeenInitialized = true;
        
            EnableControls();
        }

        private void EnableControls()
        {
            if (!_hasBeenInitialized) { InitializeControls(); }
        
            _actions.Enable();
        
            _actions.Move.started += MoveAction;
            _actions.Move.performed += MoveAction;
            _actions.Move.canceled += MoveAction;

            _actions.Look.started += LookAction;
            _actions.Look.performed += LookAction;
            _actions.Look.canceled += LookAction;

            _hasBeenEnabled = true;
        }

        private void DisableControls()
        {
            if (!_hasBeenInitialized || !_hasBeenEnabled) { return; }
        
            _actions.Disable();
        
            _actions.Move.started -= MoveAction;
            _actions.Move.performed -= MoveAction;
            _actions.Move.canceled -= MoveAction;

            _actions.Look.started -= LookAction;
            _actions.Look.performed -= LookAction;
            _actions.Look.canceled -= LookAction;

            _hasBeenEnabled = false;
        }

        private void OnEnable()
        {
            EnableControls();
        }

        private void OnDisable()
        {
            DisableControls();
        }

        private void MoveAction(InputAction.CallbackContext ctx)
        {
            OnMoveInput?.Invoke(ctx.ReadValue<Vector2>());
        }

        private void LookAction(InputAction.CallbackContext ctx)
        {
            OnLookInput?.Invoke(ctx.ReadValue<Vector2>());
        }
    }
}