using System;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

namespace Character
{
    public class CharacterManager : NetworkBehaviour
    {
        public static CharacterManager Instance
        {
            get
            {
                if (_instance != null) return _instance;
                var newCharacterManager = new GameObject("CharacterManager");
                newCharacterManager.AddComponent<CharacterManager>();
                _instance = newCharacterManager.GetComponent<CharacterManager>();
                return _instance;
            }
        }
        private static CharacterManager _instance;
        
        [SerializeField] private GameObject _characterPrefab;
        private Dictionary<int, GameObject> _spawnedCharacters = new Dictionary<int, GameObject>();

        public bool AllowSpawnCharacter = true;
        
        private void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(this);
                return;
            }

            _instance = this;
        }

        public override void OnStartServer()
        {
            base.OnStartServer();
            NetworkServer.OnDisconnectedEvent += HandleDisconnect;
        }

        public override void OnStopServer()
        {
            base.OnStopServer();
            NetworkServer.OnDisconnectedEvent -= HandleDisconnect;
        }

        [Command(requiresAuthority = false)]
        public void RequestSpawnCharacter(NetworkConnectionToClient sender = null)
        {
            if (sender == null)
            {
                Debug.LogError("Sender was null!");
                return;
            } 
            if (AllowSpawnCharacter)
            {
                Debug.Log($"{sender.connectionId} requested a character!");
                SpawnCharacter(sender.connectionId);
                return;
            }
            Debug.Log("Server doesn't allow spawning new characters!");
        }

        [Server]
        public void SpawnCharacter(int connId)
        {
            if (!CanSpawnCharacterForId(connId)) { return; }

            NetworkConnectionToClient _connection = NetworkServer.connections[connId];
            
            var newCharacter = Instantiate(_characterPrefab);
            newCharacter.name = $"Character:{connId}";
            NetworkServer.Spawn(newCharacter, _connection);

            CharacterComposer composer = newCharacter.GetComponent<CharacterComposer>();
            composer.RpcEnableCharacter();

            _spawnedCharacters.Add(connId, newCharacter);
            Debug.Log($"Spawned new character for {connId}");
        }
        
        [Server]
        public void DespawnCharacter(int connId)
        {
            if (!_spawnedCharacters.ContainsKey(connId))
            {
                Debug.LogError($"Tried to despawn non-existing character at {connId}");
                return;
            }

            if (_spawnedCharacters[connId] != null)
            {
                NetworkServer.UnSpawn(_spawnedCharacters[connId]);
                Destroy(_spawnedCharacters[connId]);
            }

            _spawnedCharacters.Remove(connId);
            Debug.Log($"Despawned character for {connId}.");
        }

        private bool CanSpawnCharacterForId(int connId)
        {
            if (_spawnedCharacters.ContainsKey(connId))
            {
                if (IsCharacterNotNull(connId))
                {
                    Debug.LogError($"{connId} already has spawned character!");
                    return false;
                }
                
                Debug.LogError($"{connId} wasn't properly despawned!");
                _spawnedCharacters.Remove(connId);
            }

            return true;
        }

        private bool IsCharacterNotNull(int connId)
        {
            return _spawnedCharacters[connId] != null;
        }

        private void HandleDisconnect(NetworkConnectionToClient conn)
        {
            if (_spawnedCharacters.ContainsKey(conn.connectionId))
            {
                Debug.Log($"Client with character disconnected! Removing character for {conn.connectionId}");
                DespawnCharacter(conn.connectionId);
            }
        }
    }
}