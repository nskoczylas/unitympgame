using System.Collections;
using System.Collections.Generic;
using Character.Components;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;
using UnityEngine.TestTools;

public class GroundProbeTests
{
    private GameObject _ground;
    private GameObject _character;
    private CharacterGroundProbe _groundProbe;

    [SetUp]
    public void Setup()
    {
        var characterPrefab = (GameObject)AssetDatabase.LoadAssetAtPath("Assets/NetworkPrefabs/Character.prefab", typeof(GameObject));
        _character = GameObject.Instantiate(characterPrefab);
        _groundProbe = _character.GetComponent<CharacterGroundProbe>();
        _groundProbe.EnableComponent();

        _ground = GameObject.CreatePrimitive(PrimitiveType.Plane);
    }

    [TearDown]
    public void TearDown()
    {
        GameObject.Destroy(_ground);
        GameObject.Destroy(_character);
    }
    
    [UnityTest]
    public IEnumerator IsGrounded_ShouldBeTrue()
    {
        // GIVEN
        
        // WHEN
        _ground.transform.position = new Vector3(0, 0, 0);
        yield return new WaitForSeconds(0.1f);
        
        // THEN
        Assert.IsTrue(_groundProbe.IsGrounded);
    }
    
    [UnityTest]
    public IEnumerator IsGrounded_ShouldBeFalse()
    {
        // GIVEN
        
        // WHEN
        _ground.transform.position = new Vector3(0, 5, 0);
        yield return new WaitForSeconds(0.1f);
        
        // THEN
        Assert.IsFalse(_groundProbe.IsGrounded);
    }
}
