using Character.Components;
using Character.Components.ActivationStrategy;
using NUnit.Framework;

public class OwnerActivationStrategyTests
{
    [Test]
    public void Accept_Owner_True()
    {
        // GIVEN
        var ownerStrategy = new OwnerActivationStrategy();

        // WHEN
        var accepted = ownerStrategy.Accept(CharacterComponentType.OwnerOnly, CharacterComponentType.OwnerOnly);
        
        // THEN
        Assert.IsTrue(accepted);
    }
    
    [Test]
    public void Accept_Shared_False()
    {
        // GIVEN
        var ownerStrategy = new OwnerActivationStrategy();

        // WHEN
        var accepted = ownerStrategy.Accept(CharacterComponentType.Shared, CharacterComponentType.OwnerOnly);
        
        // THEN
        Assert.IsFalse(accepted);
    }
    
    [Test]
    public void Accept_Server_False()
    {
        // GIVEN
        var ownerStrategy = new OwnerActivationStrategy();

        // WHEN
        var accepted = ownerStrategy.Accept(CharacterComponentType.ServerOnly, CharacterComponentType.OwnerOnly);
        
        // THEN
        Assert.IsFalse(accepted);
    }
}