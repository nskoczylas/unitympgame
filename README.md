- [SOURCE CODE](https://gitlab.com/nskoczylas/unitympgame/-/tree/master/Assets/Scripts "SOURCE CODE")
- [TESTS](https://gitlab.com/nskoczylas/unitympgame/-/tree/master/Assets/Tests "TESTS")


------------
![](https://gitlab.com/nskoczylas/unitympgame/-/raw/master/Gify/MP.gif)

Multiplayer z wysokopoziomową biblioteką [Mirror](https://assetstore.unity.com/packages/tools/network/mirror-129321 "Mirror")

------------
![](https://gitlab.com/nskoczylas/unitympgame/-/raw/master/Gify/Connect.gif)

Ruch i animacje gracza (CLIENT AUTHORITATIVE)

------------
![](https://gitlab.com/nskoczylas/unitympgame/-/raw/master/Gify/AdjustToSlope.gif)

Dostosowanie ruchu do nachylenia terenu.

------------
![](https://gitlab.com/nskoczylas/unitympgame/-/raw/master/Gify/Gravity.gif)

Sprawdzanie uziemienia + grawitacja.

------------
![](https://gitlab.com/nskoczylas/unitympgame/-/raw/master/Gify/Interaction.gif)

Początek systemu interakcji niezależny od kontrolera postaci.
